<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'API'], function () {

    Route::post('/OurHotels',          'HotelController@ourHotels');
    Route::post('/TopHotels',          'HotelController@TopHotels');
    Route::get('/BestHotel/{id}',      'HotelController@BestHotel')->name('view_hotel_by_id');
    Route::post('/store/provider',     'ProviderController@storeProvider')->name('provider.create');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

