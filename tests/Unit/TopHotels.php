<?php

namespace Tests\Unit;

use App\Http\Controllers\API\HotelController;
use App\Models\Hotels;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;



use App\Models\Provider;


class TopHotels extends TestCase
{
//    use DatabaseMigrations;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test_can_list_top_hotels() {
        $data = [
            'city' => 'AUH',
            'from_date' => '21-03-2020',
            'to_date_date' => '23-03-2021',
            'numberOfAdults' => 5,

        ];
        $this->post(url('api/TopHotels'), $data)
            ->assertStatus(200);
    }

    public function test_can_get_best_hotel_by_id() {
        $id= [
            'id' => 1,

        ];
        $this->get(route('view_hotel_by_id' , $id))
            ->assertStatus(200);
    }

}
