<?php

namespace Tests\Unit;

use App\Http\Controllers\API\HotelController;
use App\Models\Hotels;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;


use App\Models\Provider;


class TopHotels extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test_can_create_provider() {
        $data = [
            'name' => 'test',

        ];
        $this->post(url('api/store/provider'), $data)
            ->assertStatus(200);
    }

}
