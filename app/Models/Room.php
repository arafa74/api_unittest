<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = "rooms";
    public function room_amenties()
    {
        return $this->hasMany(RoomAmenity::class , 'room_id');
    }
}
