<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomAmenity extends Model
{
    protected $table = "room_amenities";
    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
