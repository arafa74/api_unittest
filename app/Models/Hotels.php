<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotels extends Model
{
    protected $table='hotels';
    protected $guarded = ['id' , 'created_at' , 'updated_at'];
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
    public function amenities()
    {
        return $this->hasMany(Amenity::class , 'hotel_id');
    }

    public function rooms()
    {
        return $this->hasMany(Room::class , 'hotel_id');
    }
    public function getRateStars()
    {
        $stars = "";
        if ($this->avg_rate == 5){
            $stars =  "*****";
        }
        if ($this->avg_rate == 4){
            $stars =  "****";
        }
        if ($this->avg_rate == 3){
            $stars =  "***";
        }
        if ($this->avg_rate == 2){
            $stars=  "**";
        }
        if ($this->avg_rate == 1){
            $stars =  "*";
        }
        return $stars;
    }
}
