<?php

namespace App\Http\Resources\API;

use App\Models\Amenity;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class Hotels extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'provider_name'=>$this->provider->name,
            'name'=>$this->name,
            'fare_price'=>$this->fare_price,
            'created_at'=>$this->created_at->format('Y-m-d'),
            'amenities'=>Arr::pluck(AmenitiesResource::collection($this->amenities) , 'name'),
        ];
    }
}
