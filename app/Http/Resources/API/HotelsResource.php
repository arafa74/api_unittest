<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class HotelsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'hotelFare'=>number_format((float)$this->fare_price, 2, '.', ''),
            'hotelRate'=>$this->avg_rate,
            'rooms'=>rooms::collection($this->rooms),

        ];
    }
}
