<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class TopHotels extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'hotelName'=>$this->name,
            'price'=>$this->fare_price,
            'rate'=>$this->getRateStars(),
            'discount'=>$this->discount?$this->discount:"No available discount",
            'created_at'=>$this->created_at->format('Y-m-d'),
            'amenities'=>Arr::pluck(AmenitiesResource::collection($this->amenities) , 'name'),
        ];
    }
}
