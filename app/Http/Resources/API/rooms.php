<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class rooms extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'room_num'=>$this->room_unm,
            'room_type'=>$this->room_type,
            'room_amenities'=>Arr::pluck(AmenitiesResource::collection($this->room_amenties), 'name'),
        ];
    }
}
