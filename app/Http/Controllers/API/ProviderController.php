<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FCM_CONTROLLER;
use App\Http\Requests\API\StoreProvider;
use App\Models\Provider;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    public function storeProvider(StoreProvider $request){
        $provider = Provider::create($request->all());
        $data['status_code'] = 200;

//     SEND NOTIFICATIONS TO USERS TO INFORM THEM THAT NEW PROVIDER ADDED


//        $fcm_data['title'] ="new provider";
//        $fcm_data['body'] = $provider->name . " added successfully";
//        $fcm_data['key'] = "new";
//        FCM_CONTROLLER::SEND_SINGLE_NOTIFICATION("users IDS how recivere notifications" , "new provider added" , $provider->name . " added successfully", $fcm_data ,  (60 * 20));

        $data['data'] = $provider->name . " added successfully";
        return response()->json($data, 200);
    }
}
