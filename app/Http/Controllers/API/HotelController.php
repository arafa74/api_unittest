<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\HotelsResource;
use App\Http\Resources\API\TopHotels;
use App\Models\Hotels;
use Illuminate\Http\Request;


class HotelController extends controller
{
    public function ourHotels(Request $request){
        $from = $request->from_date;
        $to = $request->to_date;
        $our_hotels=Hotels::when($request->city ,function($q) use ($request){
            $q->where('city', $request['city']);
        })->when($request->adults_number ,function($q) use ($request){
            $q->where('adults_number', $request['adults_number']);
        })->when($request->from_date ,function($q) use ($from){
            $q->whereDate('created_at', '>=', $from);
        })->when($request->to_date ,function($q) use ($to){
            $q->whereDate('created_at', '<=', $to);
        })->orderBy('avg_rate' , 'DESC')->get(); // data ordered by better rate
        $data['status_code'] = 200;
        $data['data'] = \App\Http\Resources\API\Hotels::collection($our_hotels);
        return response()->json($data, 200);
    }

    public function BestHotel($HotelID){
        $best_hotel=Hotels::where('id' , $HotelID)->first();
        if(!$best_hotel){
            $this->data['status_code'] = 403;
            $this->data['data']=null;
            return response()->json($this->data, 403);
        }
        $data['status_code'] = 200;
        $data['data'] =new HotelsResource($best_hotel);
        return response()->json($data, 200);
    }

    public function TopHotels(Request $request){
        $from = $request->from_date;
        $to = $request->to_date;
        $top_hotels=Hotels::when($request->city ,function($q) use ($request){
            $q->where('city', $request['city']);
        })->when($request->numberOfAdults ,function($q) use ($request){
            $q->where('adults_number', $request['numberOfAdults']);
        })->when($request->from_date ,function($q) use ($from){
            $q->whereDate('created_at', '>=', $from);
        })->when($request->to_date ,function($q) use ($to){
            $q->whereDate('created_at', '<=', $to);
        })->where('provider_id' , 2)->orderBy('avg_rate' , 'DESC')->get(); // data ordered by better rate
        $data['status_code'] = 200;
        $data['data'] = TopHotels::collection($top_hotels);
        return response()->json($data, 200);
    }
}
