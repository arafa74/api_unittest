<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 1,
            'room_type' => "single",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 2,
            'room_type' => "single",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 3,
            'room_type' => "single",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 4,
            'room_type' => "single",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 1,
            'room_type' => "double",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 2,
            'room_type' => "double",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 3,
            'room_type' => "double",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('rooms')->insert([
            'room_unm' => "#".rand(000 ,999),
            'hotel_id' => 4,
            'room_type' => "double",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

    }
}
