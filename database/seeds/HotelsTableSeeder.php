<?php

use Illuminate\Database\Seeder;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hotels')->insert([
            'name' => 'hotel A',
            'provider_id' => 3,
            'discount' => null,
            'avg_rate' => 3,
            'adults_number' => 3,
            'city' => "AUH",
            'fare_price' => "400",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('hotels')->insert([
            'name' => 'hotel B',
            'provider_id' => 4,
            'discount' => 5,
            'avg_rate' => 4,
            'adults_number' => 6,
            'city' => "OMSJ",
            'fare_price' => "300",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('hotels')->insert([
            'name' => 'hotel C',
            'provider_id' => 3,
            'discount' => 10,
            'avg_rate' => 1,
            'adults_number' => 7,
            'city' => "OMAL",
            'fare_price' => "100",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('hotels')->insert([
            'name' => 'hotel D',
            'provider_id' => 4,
            'discount' => null,
            'avg_rate' => 2,
            'adults_number' => 10,
            'city' => "AUH",
            'fare_price' => "200",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
