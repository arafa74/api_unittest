<?php

use Illuminate\Database\Seeder;

class RoomsAmenitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('room_amenities')->insert([
            'room_id' => 1,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 1,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 2,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 2,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 3,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 3,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 4,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 4,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 5,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 5,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 6,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 6,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 7,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 7,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 8,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('room_amenities')->insert([
            'room_id' => 8,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
