<?php

use Illuminate\Database\Seeder;

class hotelsAmenitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('amenities')->insert([
            'hotel_id' => 1,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('amenities')->insert([
            'hotel_id' => 1,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('amenities')->insert([
            'hotel_id' => 2,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('amenities')->insert([
            'hotel_id' => 2,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('amenities')->insert([
            'hotel_id' => 3,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('amenities')->insert([
            'hotel_id' => 3,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('amenities')->insert([
            'hotel_id' => 4,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('amenities')->insert([
            'hotel_id' => 4,
            'name' => "amenity_". rand(00 , 99),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
